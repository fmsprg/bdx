from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from django.http import JsonResponse
import json
import pprint

# Create your views here.
@csrf_exempt
def mainview(request):

    if request.method == "POST":

        

        postDict = request.POST

        if request.POST['action'] == 'create':
            pass

        if request.POST['action'] == 'remove':
            pass

        if request.POST['action'] == 'edit':
            ind = request.POST
            pp = pprint.PrettyPrinter(indent=4)
            pp.pprint( ind )
            rezDict = {}
            rezList = []
            intermediateRezDict={}
            oldnumid = -1
            for key in ind:
                if key != 'action':
                    keyarr = key.split("[")
                    rowid = keyarr[1].replace( "]", "" ).replace( "[", "" ) 
                    field = keyarr[2].replace( "]", "" ).replace( "[", "" ) 
                    numid = int("".join([x for x in [char for char in keyarr[1].replace( "]", "" ).replace( "[", "" )] if x.isdigit()]))
                    value = ind[key]
                    print(numid, field, value)
                    
                    if oldnumid != numid:
                        #this is the signal of the start of a new row.
                        #cache intermediateRezDict into rezDict, and reset it.
                        if intermediateRezDict:
                            rezList.append( intermediateRezDict )
                        intermediateRezDict={}
                        intermediateRezDict['DT_RowId']=rowid
                        oldnumid = numid
                    intermediateRezDict[field] = value
            rezList.append( intermediateRezDict )

        finalOutDict = {"data":rezList}
        pp.pprint(finalOutDict) # Uncomment to debug the final dict sent to the client.
        #strdata = '{"data": [{"DT_RowId":"row_5","first_name": "Fiona","last_name":  "Sato","position":"Chief Operating Officer (COO)","office":"San Francisco","extn":"2947","salary":"850000","start_date": "2010-03-11"}]}'
        #jsndata = json.loads(strdata)
        #pp.pprint(jsndata)
        data = {'data':{'res':'post request'}}
        return JsonResponse(finalOutDict)
        #return HttpResponse("{'res':'true'}")        

    #return render(request, 'uibasic/mainview1.html', {'id': 1})
    return render(request, 'uibasic/mainview2.html', {'id': 1})

def view_a(request):
    return render(request, 'uibasic/view_a.html', {'id': 1})

def view_b(request):
    return render(request, 'uibasic/view_b.html', {'id': 1})

def view_c(request):
    return render(request, 'uibasic/view_c.html', {'id': 1})

def dataone(request):
    return render(request, 'uibasic/dataone.html', {'id': 1})

def formsjs(request):
    return render(request, 'uibasic/dataTables.editor.min.js', {'id': 1})
    
@csrf_exempt
def mainviewPostUpdate01(request):
    data = {'res':'true'}
    return JsonResponse(data)
    #return HttpResponse("{'res':'true'}")