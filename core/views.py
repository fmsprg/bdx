from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User, Group

from rest_framework.authentication import BasicAuthentication, SessionAuthentication


from django.contrib.auth.decorators import user_passes_test,login_required

# 	1. Basic type of Auth - use this when you
#	are providing an API back end to a web site that
#	logged into with Python auth.
class MyBasicAuthentication(BasicAuthentication):

	def authenticate(self, request):
		try:
			user, _ = super(MyBasicAuthentication, self).authenticate(request)
			login(request, user)
			return user, _
		except:
			content = {'message': 'Hello, World!'}
			return Response(content)

#	2. View-class for working with (1.)
#	Checks against Django auth.
class HelloViewWebPage(APIView):

    authentication_classes = (SessionAuthentication, MyBasicAuthentication)
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        content = {
            'user': str(request.user),
            'auth': str(request.auth),  # None
        }
        return Response(content)


#	3. View-class for working against 'real' API
#	i.e where there is no concept of 'Django Log In'
#	and a token must be used.
class HelloViewAPI(APIView):
	permission_classes = (IsAuthenticated,) 


	def is_member(self, user):
		return user.groups.filter(name='testgroup1').exists()

	def is_in_one_of_many_groups(self, user):
		return user.groups.filter(name__in=['testgroup1', 'testgroup2']).exists()

	def get(self, request):
		
		#typical curl to test
		#
		#curl -X GET http://127.0.0.1:8000/api/core/hello/ -H 'Authorization: Token 85e13d0a807d133cce8cf84e66820873ff9aec2f'

		content = {'message': 'Pre-Authentication'}
		return Response(content)
		
		user_id = Token.objects.get(key=request.auth.key).user_id
		user = User.objects.get(id=user_id)    	
		print(user.groups.all())
		print(self.is_member(user))
		print(self.is_in_one_of_many_groups(user))

		content = {'message': 'Hello, World!'}
		return Response(content)
