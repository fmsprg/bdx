from django.urls import path
from rest_framework.authtoken.views import obtain_auth_token


from . import views

urlpatterns = [
    #path('', views.index, name='index'),
    path('hello/', views.HelloViewAPI.as_view(), name='hello'),
    path('web/', views.HelloViewWebPage.as_view(), name='hello'),
    path('api-token-auth/', obtain_auth_token, name='api_token_auth'),
]