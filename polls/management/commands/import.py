from django.core.management.base import BaseCommand, CommandError
from django.conf import settings 

import xml.dom.pulldom as pulldom
from xml.dom.minidom import parse, parseString
import xml.dom.minidom
import re
import gzip
import io
import requests
import xml.etree.ElementTree as ET
import datetime
import sys
import os
import html2text
#import esconfig
import redis
from polls.management.commands.redisops import BRUID




class Command(BaseCommand):

    help = 'Executes a Command'


    def remove_img_tags(data):
        p = re.compile(r'<img.*?/>')
        return p.sub('', data)


    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        #r=redis.Redis()
        print ("Executing a Command!!")
        print( settings.ESCONFIG )

        url = "http://127.0.0.1:9999/jobg8test.xml" 
        filename = "temp"
        goodIDs = []

        cmd = ""
        cmd = cmd + "curl "+ url +" > " + filename
        cmd = cmd + ""
        print(cmd)
        os.system(cmd)


        valuesArray = []

        dtnow = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
        print("----REPORT ON mini---")

        insertedRecords = []
        insertedRecordsNOT = []
        insertedRecordsCount = 0
        insertedRecordsCountNOT = 0

        tree = ET.parse(filename)
        root = tree.getroot()

        agent = "internal"
        countryCode = "CC"
        agentindex = countryCode + agent

        i=0
        j=0
        k=0

        for neighbor in root.iter('job'):
            i=i+1
            j=j+1
            
            #print( ET.tostring( neighbor[0]) )
            
            reference   =  neighbor.find('id').text 
            title       =  neighbor.find('title').text 
            description =  neighbor.find('description').text 
            URL         =  neighbor.find('url').text 
            employer    =  neighbor.find('employer').text 
            org_name    =  neighbor.find('org_name').text 
            company     =  neighbor.find('company').text 
            postdate    =  neighbor.find('postdate').text 
            city        =  neighbor.find('city').text 
            location    =  neighbor.find('location').text 
            state       =  neighbor.find('state').text 
            postalcode  =  neighbor.find('postalcode').text 
            country     =  neighbor.find('country').text 
            salary      =  neighbor.find('salary').text 
            jobtype     =  neighbor.find('jobtype').text 
            cpc         =  neighbor.find('cpc').text 
            
            new_bruid   = BRUID.lookupormakebruid(agentindex + "AND" + reference)
            #print(jobtype , " - ", new_bruid)

            description =  html2text.html2text(description)
            description = description.replace("\n\n", "---DOUBLE-RETURN---")
            description = description.replace("\n", " ")
            description = description.replace("---DOUBLE-RETURN---", "\n")
            
            newrecord={
                "new_bruid" :  new_bruid,
                "agent" : agent,
                "reference" :  reference,
                "title" :  title,
                "description" :  description,
                "city" :  city,
                "state" :  state,
                "country" :  country,
                "employer" :  employer,
                #"employertype" :  employertype,
                #"category" :  category,
                "URL" :  URL,
                #"geoLat" :  geoLat,
                #"geoLong" :  geoLong,
                "countryCode" :  countryCode,
                "cpc" :  cpc,
                "posteddate" :  postdate[:10],
                "dtnow" :  dtnow
            }
            print(newrecord)

                
        print("TOTAL RECORDS: " + str(j))
        os.system("rm " + filename) 
