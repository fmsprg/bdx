import redis
import datetime
import time
import random
import string

class BRUID():

    def randStr(chars = string.ascii_uppercase + string.digits, N=10):
        return ''.join(random.choice(chars) for _ in range(N))

    r=redis.Redis()

    def lookupormakebruid(bruid):
        #print(bruid)
        lookup = BRUID.r.get(bruid)

        #lookup = None

        #print(lookup)
        if(lookup != None):
            bruid = lookup.decode()
            #print("\t\t\tBRUID FOUND")
        else:
            #print("BRUID NOT FOUND---------")
            rst = (BRUID.randStr(N=16, chars=string.ascii_lowercase))
            bruid = datetime.datetime.utcnow().strftime('%Y%m%d%H%M%S%f')[:-1]
            bruid = rst + "AND" + bruid 
            #print(bruid)
        #time.sleep(4)
        return(bruid)
