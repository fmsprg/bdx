from django.http import HttpResponse
from django.http import HttpResponseRedirect

from django.contrib.auth.models import User, Group
from django.contrib.auth.decorators import login_required
from django.shortcuts import render

def is_member(user):
	return user.groups.filter(name='testgroup1').exists()

def is_in_one_of_many_groups(user):
	return user.groups.filter(name__in=['testgroup1', 'testgroup2']).exists()

def index(request):
	"""
	user = authenticate(username='john', password='secret')
	if user is not None:
	    # A backend authenticated the credentials
	else:
	    # No backend authenticated the credentials
	"""


	if request.user.is_authenticated:
		print("USER IS AUTHENTICATED")
	else:
		print("USER IS SNEAKY NINJA")	
		return HttpResponseRedirect('/admin/login/?next=%s' % request.path)

	user = request.user
	print(is_in_one_of_many_groups(user))
	#return HttpResponse("blurgh")
	return render(request, 'testweb/view_a.html', {'id': 1})
@login_required
def my_view(request):
	return HttpResponse("You must be logged in!!")
