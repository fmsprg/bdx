# mysite/urls.py
from django.conf.urls import include
from django.urls import path
from django.contrib import admin

from core import views


#REMOVE THIS in Production and serve static using nginx. (and see the last line in this file)
from django.contrib.staticfiles.urls import staticfiles_urlpatterns


admin.site.site_header = 'DX Administration'
admin.site.site_title = 'DX Administration'
admin.site.site_url = 'http://bdx.ai/'
admin.site.index_title = 'DX Administration'
admin.empty_value_display = '**Empty**'


urlpatterns = [
    path('chat/', include('chat.urls')),
    path('admin/', admin.site.urls),

    path('polls/', include('polls.urls')),
    path('api/core/', include('core.urls')),
    path('testweb/', include('testweb.urls')),
    
    path('ui/', include('uibasic.urls')),
    path('books/', include('book.urls')),
    path('', include('home.urls')),
] 

#REMOVE THIS in Production and serve static using nginx.

urlpatterns += staticfiles_urlpatterns()
print(staticfiles_urlpatterns())
